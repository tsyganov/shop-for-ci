let config = {
  apiUrl: 'http://localhost:8000/',
  facebookAppId: '480233909061685'
};

const env = process.env.REACT_APP_ENV || 'development';

switch (env) {
  case 'test':
    config.apiUrl = 'http://localhost:8010/';
    break;
  case 'production':
    config.apiUrl = 'http://37.139.10.106:8000/';
    break;
  default:
}

export default config;