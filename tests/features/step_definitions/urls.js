const baseUrl = 'http://localhost:3010';

module.exports = {
  loginUrl: baseUrl + '/login',
  registerUrl: baseUrl + '/register'
};