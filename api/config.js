const path = require('path');

const rootPath = __dirname;

const devDb = 'mongodb://localhost:27017/shop';
const testDb = 'mongodb://localhost:27017/shop_test';

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, '/public/uploads'),
  db: {
    url: process.env.APP_ENV === 'test' ? testDb : devDb
  },
  jwt: {
    secret: 'some kinda very secret string',
    expires: 3600
  },
  facebook: {
    appId: "480233909061685", // Enter your app ID here
    appSecret: "89879cccabc648efa8ac4099a3597230" // Enter your app secret here
  }
};

